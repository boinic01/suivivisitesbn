package fr.llb.sio.slam.suivivisitesgsb;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class SaisirVisitesTestException {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void saisirVisitesTestException() {
        ViewInteraction imageView = onView(
                allOf(withId(R.id.imgAuth),
                        withParent(allOf(withId(R.id.tableRow1),
                                withParent(withId(R.id.tableLayout)))),
                        isDisplayed()));
        imageView.perform(click());

        ViewInteraction editText = onView(
                allOf(withId(R.id.login), isDisplayed()));
        editText.perform(replaceText("ecadic"), closeSoftKeyboard());

        ViewInteraction editText2 = onView(
                allOf(withId(R.id.mdp), isDisplayed()));
        editText2.perform(replaceText("6u8dc"), closeSoftKeyboard());

        ViewInteraction button = onView(
                allOf(withId(R.id.buttonAuthentification), withText("Se connecter"), isDisplayed()));
        button.perform(click());

        ViewInteraction imageView2 = onView(
                allOf(withId(R.id.imgImport),
                        withParent(allOf(withId(R.id.tableRow3),
                                withParent(withId(R.id.tableLayout)))),
                        isDisplayed()));
        imageView2.perform(click());

        ViewInteraction button2 = onView(
                allOf(withId(R.id.buttonImporter), withText("Importer"), isDisplayed()));
        button2.perform(click());

        ViewInteraction imageView3 = onView(
                allOf(withId(R.id.imgVisites),
                        withParent(allOf(withId(R.id.tableRow1),
                                withParent(withId(R.id.tableLayout)))),
                        isDisplayed()));
        imageView3.perform(click());

        pressBack();

    }

}
