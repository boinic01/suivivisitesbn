package fr.llb.sio.slam.suivivisitesgsb;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class SaisirVisitesTestAlternatif2 {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void saisirVisitesTestAlternatif2() {
        ViewInteraction imageView = onView(
                allOf(withId(R.id.imgAuth),
                        withParent(allOf(withId(R.id.tableRow1),
                                withParent(withId(R.id.tableLayout)))),
                        isDisplayed()));
        imageView.perform(click());

        ViewInteraction editText = onView(
                allOf(withId(R.id.login), isDisplayed()));
        editText.perform(replaceText("jcolonna "), closeSoftKeyboard());

        ViewInteraction editText2 = onView(
                allOf(withId(R.id.mdp), isDisplayed()));
        editText2.perform(replaceText("1234"), closeSoftKeyboard());

        ViewInteraction button = onView(
                allOf(withId(R.id.buttonAuthentification), withText("Se connecter"), isDisplayed()));
        button.perform(click());

        ViewInteraction imageView2 = onView(
                allOf(withId(R.id.imgImport),
                        withParent(allOf(withId(R.id.tableRow3),
                                withParent(withId(R.id.tableLayout)))),
                        isDisplayed()));
        imageView2.perform(click());

        ViewInteraction button2 = onView(
                allOf(withId(R.id.buttonImporter), withText("Importer"), isDisplayed()));
        button2.perform(click());

        ViewInteraction imageView3 = onView(
                allOf(withId(R.id.imgVisites),
                        withParent(allOf(withId(R.id.tableRow1),
                                withParent(withId(R.id.tableLayout)))),
                        isDisplayed()));
        imageView3.perform(click());

        ViewInteraction linearLayout = onView(
                allOf(childAtPosition(
                        withId(R.id.listeVisites),
                        2),
                        isDisplayed()));
        linearLayout.perform(click());

        ViewInteraction switch_ = onView(
                allOf(withId(R.id.presenceProspect),
                        withParent(withId(R.id.tableRowPresence))));
        switch_.perform(scrollTo(), click());

        ViewInteraction editText3 = onView(
                allOf(withId(R.id.motifVisite),
                        withParent(withId(R.id.tableRowMotifVisite))));
        editText3.perform(scrollTo(), replaceText("Motif"), closeSoftKeyboard());

        ViewInteraction radioButton = onView(
                allOf(withId(R.id.rbLisibleOk), withText("Parfaitement lisible"),
                        withParent(allOf(withId(R.id.rgLisibilite),
                                withParent(withId(R.id.tableRowLisibilite)))),
                        isDisplayed()));
        radioButton.perform(click());

        ViewInteraction editText4 = onView(
                allOf(withId(R.id.bilan),
                        withParent(withId(R.id.tableRowBilan))));
        editText4.perform(scrollTo(), replaceText("Bilan"), closeSoftKeyboard());

        ViewInteraction button3 = onView(
                allOf(withId(R.id.validerAvis), withText("Valider")));
        button3.perform(scrollTo(), click());

        pressBack();

        ViewInteraction imageView4 = onView(
                allOf(withId(R.id.imgVisites),
                        withParent(allOf(withId(R.id.tableRow1),
                                withParent(withId(R.id.tableLayout)))),
                        isDisplayed()));
        imageView4.perform(click());

        ViewInteraction linearLayout2 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.listeVisites),
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.RelativeLayout.class),
                                        0)),
                        2),
                        isDisplayed()));
        linearLayout2.check(matches(isDisplayed()));

        ViewInteraction linearLayout3 = onView(
                allOf(childAtPosition(
                        withId(R.id.listeVisites),
                        2),
                        isDisplayed()));
        linearLayout3.perform(click());

        ViewInteraction switch_2 = onView(
                allOf(withId(R.id.presenceProspect),
                        childAtPosition(
                                allOf(withId(R.id.tableRowPresence),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.TableLayout.class),
                                                3)),
                                1),
                        isDisplayed()));
        switch_2.check(matches(isDisplayed()));

        ViewInteraction editText5 = onView(
                allOf(IsInstanceOf.<View>instanceOf(android.widget.EditText.class), withText("14"),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        0),
                                1),
                        isDisplayed()));
        editText5.check(matches(withText("14")));

        ViewInteraction editText6 = onView(
                allOf(IsInstanceOf.<View>instanceOf(android.widget.EditText.class), withText("déc."),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        1),
                                1),
                        isDisplayed()));
        editText6.check(matches(withText("déc.")));

        ViewInteraction editText7 = onView(
                allOf(IsInstanceOf.<View>instanceOf(android.widget.EditText.class), withText("2016"),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        2),
                                1),
                        isDisplayed()));
        editText7.check(matches(withText("2016")));

        ViewInteraction editText8 = onView(
                allOf(withId(R.id.motifVisite), withText("Motif"),
                        childAtPosition(
                                allOf(withId(R.id.tableRowMotifVisite),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.TableLayout.class),
                                                5)),
                                1),
                        isDisplayed()));
        editText8.check(matches(withText("Motif")));

        ViewInteraction ratingBar = onView(
                allOf(withId(R.id.confianceGSB),
                        childAtPosition(
                                allOf(withId(R.id.tableRowConfiance),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.TableLayout.class),
                                                6)),
                                1),
                        isDisplayed()));
        ratingBar.check(matches(isDisplayed()));


        ViewInteraction radioButton3 = onView(
                allOf(withId(R.id.rbLisibleOk),
                        childAtPosition(
                                allOf(withId(R.id.rgLisibilite),
                                        childAtPosition(
                                                withId(R.id.tableRowLisibilite),
                                                1)),
                                0),
                        isDisplayed()));
        radioButton3.check(matches(isDisplayed()));

        ViewInteraction editText9 = onView(
                allOf(withId(R.id.bilan), withText("Bilan"),
                        childAtPosition(
                                allOf(withId(R.id.tableRowBilan),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.TableLayout.class),
                                                8)),
                                1),
                        isDisplayed()));
        editText9.check(matches(withText("Bilan")));

        ViewInteraction button4 = onView(
                allOf(withId(R.id.validerAvis), withText("Valider")));
        button4.perform(scrollTo(), click());

        pressBack();

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
