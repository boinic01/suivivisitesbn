package fr.llb.sio.slam.suivivisitesgsb;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Julien on 09/10/2016.
 */
public class AuthentificationTest {

    @Test
    public void testSHA1() throws Exception {
        assertEquals("L'empreinte n'est pas la bonne", Authentification.SHA1("1234"), "7110eda4d09e062aa5e4a390b0a572ac0d2c0220");
    }
}