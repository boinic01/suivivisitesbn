package fr.llb.sio.slam.suivivisitesgsb.vues;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import fr.llb.sio.slam.suivivisitesgsb.AfficheVisite;
import fr.llb.sio.slam.suivivisitesgsb.R;
import fr.llb.sio.slam.suivivisitesgsb.metier.Visite;
import fr.llb.sio.slam.suivivisitesgsb.modele.Modele;

public class AfficheListeVisites extends Activity {

    private ListView listView;
    private List<Visite> listeVisites;
    private Modele modele = Modele.getModele();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affiche_liste_visites);

        // Affichage de la liste de visites
        listeVisites = modele.listeVisites();
        listView = (ListView) findViewById(R.id.listeVisites);
        VisiteAdapter adaptVisite = new VisiteAdapter(this, listeVisites);
        listView.setAdapter(adaptVisite);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Toast.makeText(getApplicationContext(), "Choix : " + listeVisites.get(position).getId(), Toast.LENGTH_SHORT).show();

                // Appel de l'activité AfficheVisite
                Intent myIntent = new Intent(getApplicationContext(), AfficheVisite.class);
                myIntent.putExtra("paramIdVisite", listeVisites.get(position).getId());
                startActivity(myIntent);
            }
        });
    }
}
