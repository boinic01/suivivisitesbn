package fr.llb.sio.slam.suivivisitesgsb.metier;

import java.util.Date;

public class Visite {

    //Données ne pouvant pas être modifiées
    private String id, nom, prenom, adresse, tel;

    // Données à saisir lors de la visite
    private Date date;
    private String motif;
    private float niveauConfiance;  // Niveau de confiance pour GSB évalué sur 5
    private String lisibilite;      // Lisibilité des notices
    private String bilan;

    // Indicateur d'état
    private boolean present;

    public Visite() {
    }

    public Visite(String id, String nom, String prenom, String adresse, String telephone) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.tel = telephone;

        this.present = false;
        this.date = new Date();
        this.motif = "";
        this.niveauConfiance = 0;
        this.lisibilite = "";
        this.bilan = "";
    }

    public void copieVisite(Visite v) {
        this.id = v.id;
        this.nom = v.nom;
        this.prenom = v.prenom;
        this.adresse = v.adresse;
        this.tel = v.tel;
        this.present = v.present;
        this.date = v.date;
        this.motif = v.motif;
        this.niveauConfiance = v.niveauConfiance;
        this.lisibilite = v.lisibilite;
        this.bilan = v.bilan;
    }


    // Accesseurs
    public String getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getTelephone() {
        return tel;
    }

    public Boolean getPresent() {
        return present;
    }

    public Date getDateVisite() {
        return date;
    }

    public String getMotifVisite() {
        return motif;
    }

    public float getNiveauConfiance() {
        return niveauConfiance;
    }

    public String getLisibilite() {
        return lisibilite;
    }

    public String getBilanVisite() {
        return bilan;
    }



    // Mutateurs
    public void setId(String id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setTelephone(String telephone) {
        this.tel = telephone;
    }

    public void setPresent(Boolean present) {
        this.present = present;
    }

    public void setDateVisite(Date dateVisite) {
        this.date = dateVisite;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public void setNiveauConfiance(float niveauConfiance) {
        this.niveauConfiance = niveauConfiance;
    }

    public void setLisibilite(String lisibilite) {
        this.lisibilite = lisibilite;
    }

    public void setBilan(String bilan) {
        this.bilan = bilan;
    }


    // Méthodes
    @Override
    public String toString() {
        return "Visite{" +
                "id='" + id + '\'' +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", telephone='" + tel + '\'' +
                ", present=" + present +
                ", dateVisite=" + date +
                ", motif=" + motif +
                ", niveauConfiance=" + niveauConfiance +
                ", lisibilite='" + lisibilite + '\'' +
                ", bilan='" + bilan + '\'' +
                '}';
    }
}