package fr.llb.sio.slam.suivivisitesgsb;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    // La carte
    private GoogleMap map;

    // Attributs pour la localisation du visiteur
    private LocationManager locationManager;
    private String provider;
    private LatLng positionVisiteur;
    private boolean reussiGeolocalisationVisiteur = false;
    private Marker visiteurMarker;

    // Attribut pour la localisation de l'adresse du Prospect
    private String adresseProspect;
    private LatLng positionProspect;
    private boolean reussiGeocodageProspect = false;

    // Détermine les frontières de la carte en contenant tous les points
    private LatLngBounds.Builder builder = new LatLngBounds.Builder();

    // Codes permissions
    private static final int REQUEST_ACCESS_LOCATION = 111;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Récupére l'adresse du prospect à géocoder
        Bundle bundle = getIntent().getExtras();
        adresseProspect = bundle.getString("paramAdresse");

        // Récupère la carte
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Actions réalisées lorsque la carte est chargée
        map = googleMap;
        geolocalisationVisiteur();
        geocodageAdresseProspect();
        afficheCarte(googleMap);
    }


    private void afficheCarte(GoogleMap googleMap) {
        // Changer le type de la carte
        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        //googleMap.setMyLocationEnabled(true);  // Position de l'utilisateur sans pouvoir obtenir ses coordonnées géographiques.

        // Personnalisation IU
        UiSettings settings = googleMap.getUiSettings();
        settings.setZoomControlsEnabled(true);
        settings.setCompassEnabled(true);

        // Personnalisation de la gestion des gestes
        settings.setAllGesturesEnabled(true);
        settings.setZoomGesturesEnabled(true);
        settings.setRotateGesturesEnabled(true);
        settings.setScrollGesturesEnabled(true);
        settings.setTiltGesturesEnabled(true); // Inclinaison avec 2 doigts



        // Ajout d'un marker personnalisé sur la position du visiteur
        if (reussiGeolocalisationVisiteur) {
            visiteurMarker = googleMap.addMarker(new MarkerOptions()
                    .position(positionVisiteur)
                    .title("Ma position"));
            builder.include(positionVisiteur);
        }

        // Ajout d'un marker personnalisé sur la position du prospect
        if (reussiGeocodageProspect) {
            MarkerOptions mkOpt = new MarkerOptions();
            mkOpt.position(positionProspect);
            mkOpt.title("Prospect");
            mkOpt.snippet("Point de rendez vous de la prochaine visite");
            mkOpt.icon(BitmapDescriptorFactory.fromResource(R.drawable.punaise));
            googleMap.addMarker(mkOpt);
            builder.include(positionProspect);
        }

        // Positionnement de la caméra en fonction de la réussite des localisations
        if (reussiGeolocalisationVisiteur && reussiGeocodageProspect) {
            // Déplacement de la caméra en fonction du builder, de la taille de l'écran, du padding
            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
                    builder.build(),
                    this.getResources().getDisplayMetrics().widthPixels,
                    this.getResources().getDisplayMetrics().heightPixels,
                    350));
        }

        if (!reussiGeolocalisationVisiteur && reussiGeocodageProspect) {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(positionProspect, 15));
        }

        if (reussiGeolocalisationVisiteur && !reussiGeocodageProspect) {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(positionVisiteur, 15));
        }
    }

    // Récuperation de la position du visiteur
    public void geolocalisationVisiteur() {

        // Vérifier les permissions pour l'utilisation du GPS (nécéssaire à partir d'Android 6.0)
        if ((ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {

            // Récupère le service de localisation
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            Criteria criteria = new Criteria();

            // Choix du fournisseur de données
            // Vérifier si le module GSP est accessible
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                Toast.makeText(getBaseContext(), "GPS OK", Toast.LENGTH_SHORT).show();
                // Sélection de ce fournisseur de données
                provider = LocationManager.GPS_PROVIDER;
            } else {
                // Sinon sélection du meilleur fournisseur disponible (WiFi, 3G)
                provider = locationManager.getBestProvider(criteria, false);
            }

            if (provider != null && !provider.equals("")) {
                Toast.makeText(getBaseContext(), "Provider : " + provider, Toast.LENGTH_SHORT).show();

                // Mise à jour de localisation auprès du fournisseur
                locationManager.requestLocationUpdates(provider, 2000, 1, this);
                // Retourne la dernière localisation enregistrée de l'utilisateur
                Location location = locationManager.getLastKnownLocation(provider);

                if (location != null) {
                    positionVisiteur = new LatLng(location.getLatitude(), location.getLongitude());
                    reussiGeolocalisationVisiteur = true;
                } else {
                    Toast.makeText(getBaseContext(), "Erreur dans la géolocalisation", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getBaseContext(), "WIFI/GPS non accessibles", Toast.LENGTH_SHORT).show();
            }
        }else{
            //Permissions non accordées, demande des 2 permissions en même temps
            String[] PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
            ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_ACCESS_LOCATION);
        }

    }

    // Vérification des permissions accordées par l'utilisateur
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ACCESS_LOCATION: {
                Map<String, Integer> perms = new HashMap<>();
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);

                    // Check for both permissions
                    if (perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        // Permissions accordées
                        geolocalisationVisiteur();
                    } else
                        Toast.makeText(getApplicationContext(), "Sans votre accord vous ne pouvez pas être localisé", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }


    // Récupération de la position du prospect à partir de son adresse
    public void geocodageAdresseProspect() {

        if (!Geocoder.isPresent()) {
            Toast.makeText(getApplicationContext(), "Service Geocoder non activé", Toast.LENGTH_SHORT).show();
        } else {

            // Géocodage = trouver les coordonnées à partir de l'adresse
            Geocoder geocodage = new Geocoder(this, Locale.FRANCE);
            List<Address> locations = null;

            try {
                // Le Geocoder retourne 3 localisations à partir de l'adresse postale
                locations = geocodage.getFromLocationName(adresseProspect, 3);
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Pbm geocoder adresse prospect" + e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            // Cas ou géocoder n'a retourné aucune position à partir de l'adresse
            if ((locations == null) || (locations.isEmpty())) {
                Toast.makeText(getApplicationContext(), "Adresse prospect inconnue !", Toast.LENGTH_SHORT).show();
            } else {
                // Si Geocoder a fonctionné on récupère la première position
                positionProspect = new LatLng(locations.get(0).getLatitude(), locations.get(0).getLongitude());
                reussiGeocodageProspect = true;
            }
        }
    }

    // Méthodes implémentant l'interface LocationListener
    // Méthode qui se déclenche lorsque la position de l'utilisateur change
    @Override
    public void onLocationChanged(Location location) {
        // Mise à jour automatique du marqueur du visiteur
        if(visiteurMarker != null)
            visiteurMarker.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));
    }

    @Override
    public void onProviderDisabled(String arg0) { }

    @Override
    public void onProviderEnabled(String arg0) { }

    @Override
    public void onStatusChanged(String arg0, int arg1, Bundle arg2) { }
}
