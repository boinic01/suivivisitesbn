package fr.llb.sio.slam.suivivisitesgsb;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import fr.llb.sio.slam.suivivisitesgsb.modele.Modele;

public class Authentification extends Activity {

    private Button btnSeConnecter, btnAnnuler, btnDeconnecter;
    private EditText editTextLogin, editTextMDP;
    private String identifiant, mdp, mdpsha1;
    private AsyncTask<String, String, Boolean> connexionAsynchrone;
    private SharedPreferences jetonAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentification);

        jetonAuth = getSharedPreferences("AppData", Context.MODE_PRIVATE);

        // Récupère les boutons depuis le layout
        btnSeConnecter = (Button) findViewById(R.id.buttonAuthentification);
        btnAnnuler=(Button) findViewById(R.id.buttonAnnuler);
        btnDeconnecter=(Button) findViewById(R.id.buttonDeconnexion);

        // Gestion de l'évènement sur le clic du bouton Annuler
        btnAnnuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });

        // Gestion de l'évènement sur le clic du bouton de Déconnexion
        btnDeconnecter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                getSharedPreferences("AppData", Context.MODE_PRIVATE).edit().clear().commit();
                Modele.getModele().deleteVisites();
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });

        // Gestion de l'évènement sur le clic du bouton Authentification
        btnSeConnecter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // Récupère les informations saisies par l'utilisateur
                editTextLogin = (EditText) findViewById(R.id.login);
                editTextMDP = (EditText) findViewById(R.id.mdp);
                identifiant = editTextLogin.getText().toString();
                mdp = editTextMDP.getText().toString();
                mdpsha1 = SHA1(mdp);
                Log.i("sha1", "MDP : " + mdp + " Hash : " + mdpsha1);

                // Création d'un tableau de paramètres à passer à l'AsyncTask
                String[] mesParams = {identifiant, mdpsha1, "http://192.168.60.102/suivivisitesgsb/authentification.php"};
                //String[] mesParams = {identifiant, mdpsha1, "https://gsb.dtdns.net:8082/suivivisitesgsb/authentification.php"};
                // Lance l'AsyncTask
                connexionAsynchrone = new Connexion(Authentification.this);
                connexionAsynchrone.execute(mesParams); // --> Serveur
            }
        });
    }

    // --> Retour des données du serveur
    // Méthode éxécutée après l'AsyncTask Connexion
    public void retourVersAuthentification(StringBuilder codeRetour) {
        SharedPreferences.Editor edit = jetonAuth.edit();

        // Authentification réussie si la valeur de retour est 1 (voir script PHP)
        if (codeRetour.toString().compareTo("1") == 0) {
            Toast.makeText(this, "Authentification réussie", Toast.LENGTH_SHORT).show();

            // On stocke dans les Shared Preference le login et le mot de passe si l'utilisateur a été authentifié
            edit.putString("login", identifiant);
            edit.putString("mdp", mdpsha1);
            edit.commit();
            Modele.getModele().deleteVisites();

            setResult(Activity.RESULT_OK);
            finish();
        } else {
            // Echec Authentification
            Toast.makeText(this, "Echec authentification", Toast.LENGTH_SHORT).show();
            edit.clear();
            edit.commit();
            setResult (Activity.RESULT_CANCELED);
            finish();
        }
    }

    /**
     * Retourne l'empreinte SHA1 de la chaine de caractères passée en paramètre
     * @param text chaine de caractères dont l'empreinte va être calculée
     * @return l'empreinte SHA1 ou null si une erreur est survenue
     */
    public static String SHA1(String text) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");

            // Conversion du mdp en bytes puis calcul de l'empreinte SHA1
            md.update(text.getBytes("UTF-8"), 0, text.length());
            byte[] sha1hash = md.digest();

            // Conversion de l'empreinte en bytes en chaine de caractères hexadécimale
            return byte2Hex(sha1hash);

        } catch (NoSuchAlgorithmException e) {
            // Algorithme de hash inexistant
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // Encodage non supporté
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Convertit un tableau de bytes en chaine de caractères hexadécimale
     * @param bytes tableau de bytes à convertir en chaine de caractères héxadécimale
     * @return la chaine de caractères héxadécimale correspondante
     */
    public static String byte2Hex(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length*2);
        for (byte b : bytes) {
            // The % is an indicator of a format specifier. the 02 is the number of digits or characters to output. The x is for hex format.
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

}
